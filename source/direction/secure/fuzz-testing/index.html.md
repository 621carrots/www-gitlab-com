---
layout: markdown_page
---

<meta http-equiv="refresh" content="0; URL=.." />

This is a placeholder page for the Fuzz Testing group. Check out the
[Secure stage](https://about.gitlab.com/direction/secure/) page to see what
we are working on and our individual category pages to see what our group is
working on.